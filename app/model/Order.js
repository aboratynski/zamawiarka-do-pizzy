Ext.define('MyApp.model.Order',{
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'number'},
        {name: 'fullname', type: 'string'},
        {name: 'pizza_id', type: 'int'},
        {name: 'half', type: 'boolean'},
        {name: 'cost', type: 'number'},
        {name: 'myOrder', type: 'boolean'}
    ],
    idProperty: 'id',
    autoLoad: true,
    autoSync: true,
    proxy: {
        type: 'rest',
        id  : 'orders',
        appendId: false,
        actionMethods:{
            create: 'POST',
            read: 'GET',
            update: 'PUT',
            destroy: 'DELETE'
        },
        url : '/Orders.php',
        reader: {
            type: 'json',
            rootProperty: 'id'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});