Ext.define('MyApp.model.AgregateOrder',{
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'number'},
        {name: 'pizzaName', type: 'string'},
        {name: 'pizza_id', type: 'int'},
        {name: 'pizza_ids', type: 'string'},
        {name: 'count', type: 'number'},
        {name: 'cost', type: 'number'},
    ],

});