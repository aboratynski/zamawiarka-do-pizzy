Ext.define('MyApp.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewModel.main',

    data: {
        name: 'MyApp'
    }

    //TODO - add data, formulas and/or methods to support your view
});