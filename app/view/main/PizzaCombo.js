Ext.define('MyApp.view.main.PizzaCombo', {
    extend: 'Ext.form.ComboBox',
    xtype: "pizzaCombo",
    store: Ext.data.StoreManager.get("pizzaStore"),
    queryMode: 'local',
    displayField: 'name',
    valueField: 'id'
});