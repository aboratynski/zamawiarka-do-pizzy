Ext.define('MyApp.view.main.AggregateGrid',{
    extend: 'Ext.grid.Panel',
    title: "Zagregowane zamówienia",
    store: Ext.data.StoreManager.lookup('aggregateStore'),
    columns: [
        { text: 'Nazwa pizzy',  dataIndex: 'pizzaName', flex: 1},
        { text: 'Numerki pizzy', dataIndex: 'pizza_ids'},
        { text: 'Ilość', dataIndex: 'count', summaryType: 'sum'},
        { text: 'Koszt', dataIndex: 'cost', summaryType: 'sum'}
    ]
    //features: [{ftype: 'summary'}],
});