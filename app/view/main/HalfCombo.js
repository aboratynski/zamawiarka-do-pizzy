Ext.define('MyApp.view.main.HalfCombo', {
    extend: 'Ext.form.ComboBox',
    xtype: 'HalfCombo',
    queryMode: 'local',
    store: Ext.create('Ext.data.Store', {
        fields: ['name','val'],
        data: [
            {'name': "Połówka", 'val': true},
            {'name': "Cała", 'val': false}
        ]
    }),
    displayField: 'name',
    valueField: 'val'
});