Ext.define('MyApp.view.main.FullGrid',{
    extend: 'Ext.grid.Panel',
    requires: [
        'MyApp.view.main.PizzaCombo',
        'MyApp.view.main.HalfCombo',
    ],
    title: "Pełny widok zamówień",
    store: Ext.data.StoreManager.lookup('orderStore'),
    columns: [
        { id: "fullNameColumn", text: 'Imię i nazwisko',  dataIndex: 'fullname', width: 200, editor: 'textfield'},

        {
            text: 'Pizza',
            dataIndex: 'pizza_id',
            renderer: function(value){
                var rec = Ext.data.StoreManager.get("pizzaStore").getById(value);
                return rec.get("name");
            },
            editor: {xtype: "pizzaCombo"},
        },
        { text: 'Numer Pizzy', dataIndex: 'pizza_id', flex: 1,},
        {
            text: 'Jaka część',
            dataIndex: 'half',
            flex: 1,
            renderer: function(value){
                if(value) return "Połówka";
                return "Cała";
            },
            editor: {xtype: "HalfCombo"},
        },
        {
            text: 'Koszt',
            dataIndex: 'id',
            renderer: function(value){
                var order = Ext.data.StoreManager.get("orderStore").getById(value);
                var cost = Ext.data.StoreManager.get("pizzaStore").getById(order.data.pizza_id);
                return cost.get("cost") * ((order.data.half)? 0.5: 1);
            }
        }
    ],
    //plugins: [fullGridRowEditing],
});