Ext.define('MyApp.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'MyApp.view.main.FullGrid',
    ],
    xtype: 'app-main',

    controller: 'main',
    viewModel: 'main',

    initComponent: function(){
        var mainPanel = Ext.create('Ext.panel.Panel',{
            bodyPadding : 5,
            title       : "Zamawiarka do pizzy",
        });
        var mainPanelToolbar = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'top',
        });
        var fullgrid = Ext.create('MyApp.view.main.FullGrid');
        this.items = [fullgrid];
        this.callParent(arguments);
    }

});