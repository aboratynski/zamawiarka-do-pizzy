Ext.define('MyApp.Application', {
    extend: 'Ext.app.Application',

    requires: [
        'MyApp.view.main.Main',
        'MyApp.view.main.MainController',
        'MyApp.view.main.MainModel',
    ],
    name: 'MyApp',

    mainView: 'MyApp.view.main.Main',

    launch: function () {

    }
});