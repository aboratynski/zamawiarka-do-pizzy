Ext.define('MyApp.store.Pizza', {
    extend: 'Ext.data.Store',
    storeId:'pizzaStore',
    idProperty: 'id',
    fields: ['name', {name:'id', type: 'int'}, {name:'cost', type:'float'}],
    data: [
        {name: "MARGHERITA", id: 1, cost: "16.50"},
        {name: "BAMBOLA", id: 2, cost: "20.50"},
        {name: "CAPRICIOSA", id: 3, cost: "22.50"},
        {name: "BAMBINO", id: 4, cost: "22.50"}
    ]
});