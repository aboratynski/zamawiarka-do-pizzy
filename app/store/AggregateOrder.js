Ext.define('MyApp.store.AggregateOrder', {
    extend: 'Ext.data.Store',
    storeId:'aggregateStore',
    model: 'MyApp.model.AggregateOrder',
    sortRoot: 'pizza_id',
    sortOnLoad: true,
});