Ext.define('MyApp.store.Order', {
    extend: 'Ext.data.Store',
    model: 'MyApp.model.Order',
    storeId:'orderStore',
    sorters:
    {
        property: 'id',
        direction: 'ASC'
    },
    sortRoot: 'id',
    sortOnLoad: true,
    autoLoad: true,
    autoSync: true
});