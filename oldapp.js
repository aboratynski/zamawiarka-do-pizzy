



var reloadAggregateStore = function(){
    aggregateStore.removeAll();

    for(var i= 0; i<orderStore.data.items.length; i++){
        var loopObject = orderStore.getById(orderStore.data.items[i].data.id);
        if(aggregateStore.findRecord('pizza_id',loopObject.data.pizza_id )){
            var aggregateRow = aggregateStore.findRecord('pizza_id',loopObject.data.pizza_id );
            aggregateRow.data.cost += (pizzaStore.findRecord('id',loopObject.data.pizza_id).data.cost)*((loopObject.data.half)? 0.5: 1);
            aggregateRow.data.count += (loopObject.data.half)? 0.5: 1;
        }else{
            var aggregateRow = Ext.create('AgregateOrder',{
                'pizzaName': pizzaStore.findRecord('id',loopObject.data.pizza_id).data.name,
                'cost': (pizzaStore.findRecord('id',loopObject.data.pizza_id).data.cost)*((loopObject.data.half)? 0.5: 1),
                'pizza_id': loopObject.data.pizza_id,
                'pizza_ids': loopObject.data.pizza_id,
                'count': (loopObject.data.half)? 0.5: 1
            });
            aggregateStore.add(aggregateRow);
        }
    }
    //agregateGrid.getView().refresh();
    var halves = new Array;
    var toDrop = new Array;
    for(var j= 0; j<aggregateStore.data.items.length; j++){
        var loopObject = aggregateStore.data.items[j];
        if(loopObject.data.count%1>0){
            loopObject.data.count -= 0.5;
            loopObject.data.cost -= (pizzaStore.findRecord('id',loopObject.data.pizza_id).data.cost * 0.5);
            var aggregateRow = Ext.create('AgregateOrder',{
                'pizzaName': pizzaStore.findRecord('id',loopObject.data.pizza_id).data.name,
                'cost': (pizzaStore.findRecord('id',loopObject.data.pizza_id).data.cost)*0.5,
                'pizza_id': loopObject.data.pizza_id,
                'pizza_ids': loopObject.data.pizza_ids,
                'count': 0.5
            });
            halves.push(aggregateRow);
            if(loopObject.data.count<1){
                toDrop.push(loopObject.id);
            }
        };
    };
    for(var j= 0; j<toDrop.length; j++){
        aggregateStore.getById(toDrop[j]).drop();
    }

    while(halves.length>1){

        var firstHalf = halves.pop();
        var secondHalf = halves.pop();
        var newPizza = Ext.create('AgregateOrder',{
            'pizzaName': firstHalf.data.pizzaName + ' / ' + secondHalf.data.pizzaName,
            'cost': ((pizzaStore.findRecord('id',firstHalf.data.pizza_id).data.cost)*0.5)+((pizzaStore.findRecord('id',secondHalf.data.pizza_id).data.cost)*0.5),
            'pizza_id': 0,
            'pizza_ids': firstHalf.data.pizza_ids + ' / ' + secondHalf.data.pizza_ids,
            'count': 1
        });
        aggregateStore.add(newPizza);
    }
    if(halves.length>0){
        var half=halves.pop();
        aggregateStore.add(half);
        Ext.Msg.show({
            title:'Nieparzysta ilość połówek',
            message: 'Zamówienie składa się z nieparzystej ilości połówek. Czy chcesz powrócić do Edycji zamówień?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    fullbutton.fireEvent('click');
                } else if (btn === 'no') {
                }
            }
        });
    }
    halves=[];
};


var mainContainer = Ext.create('Ext.container.Container', {
    renderTo     : Ext.getBody(),
    bodyPadding  : 5,
});

var mainPanel = Ext.create('Ext.panel.Panel',{
    bodyPadding : 5,
    title       : "Zamawiarka do pizzy",
});

var mainPanelToolbar = Ext.create('Ext.toolbar.Toolbar', {
    dock: 'top',
});
var addButton = Ext.create('Ext.button.Button', {
    text: 'Dodaj zamówienie',
});
var deleteButton = Ext.create('Ext.button.Button', {
    text: 'Usuń zamówienie',
});
var space = Ext.create('Ext.Component',{
    flex: 1,
});

var fullbutton = Ext.create('Ext.button.Button', {
    text: 'Pełne zamówienia',
    disabled: true,

});
var agregateButton = Ext.create('Ext.button.Button', {
    text: 'Zagregowane zamówienia',
});

var fullOrderContainer = Ext.create('Ext.container.Container',{
});
var agregateOrderContainer = Ext.create('Ext.container.Container',{
    hidden: true,
});

var fullGridRowEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});





mainContainer.add(mainPanel);
mainPanel.addDocked(mainPanelToolbar);
mainPanelToolbar.add(addButton);
mainPanelToolbar.add(deleteButton);
mainPanelToolbar.add(space);
mainPanelToolbar.add(fullbutton);
mainPanelToolbar.add(agregateButton);
mainPanel.add(fullOrderContainer);
mainPanel.add(agregateOrderContainer);
fullOrderContainer.add(fullGrid);
agregateOrderContainer.add(agregateGrid);

agregateButton.on('click',function(){
    fullOrderContainer.hide();
    this.disable();
    fullbutton.enable();
    addButton.disable();
    deleteButton.disable();
    reloadAggregateStore();
    agregateOrderContainer.show();
});

fullbutton.on('click',function(){
    agregateOrderContainer.hide();
    this.disable();
    addButton.enable();
    deleteButton.enable();
    agregateButton.enable();



    fullOrderContainer.show();
});

addButton.on('click', function(){
    fullGridRowEditing.cancelEdit();


    orderStore.insert(0,{'fullname': 'Your name',  "pizza_id":"1",  "half":"1", "myOrder": "true"  });
    // orderStore.reload();
    //fullGrid.getView().refresh();
});

deleteButton.on('click', function(){
    Ext.Msg.show({
        title:'Usuwanie zamówienia',
        message: 'Czy na pewno chcesz usunąć zamówienie?',
        buttons: Ext.Msg.YESNO,
        icon: Ext.Msg.QUESTION,
        fn: function(btn) {
            if (btn === 'yes') {
                var sm = fullGrid.getSelectionModel();
                fullGridRowEditing.cancelEdit();
                var selectionRow=sm.getSelection();
                if(selectionRow[0].data.myOrder)
                {
                    orderStore.remove(sm.getSelection());

                    if (orderStore.getCount() > 0) {
                        sm.select(0);
                    }
                }
            } else if (btn === 'no') {
                return false;
            }
        }
    });

});

orderStore.on('load', function(){
    //fullGrid.getView().refresh();
});

fullGrid.on('add',function(store, e){
    var internalId = orderStore.last().internalId;
    //orderStore.reload();
    var last = orderStore.getByInternalId(internalId);
    //fullGrid.getView().refresh();
    fullGridRowEditing.startEdit(last);
});

fullGridRowEditing.on('beforeedit', function(editor,context){
    if(!context.record.data.myOrder){
        return false;
    }
});
fullGridRowEditing.on('edit', function(editor,context){
    context.record.save();
    //orderStore.reload();
    //context.grid.getView().refresh();

});


