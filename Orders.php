<?php
$username="test_user";
$password="zxcvbnm";
$database="test";
$pgsql_host="localhost";
$port="5432";
if(!isset($_COOKIE['userToken'])){
    setcookie('userToken',sha1(date("Y-m-d H:i:s")),time()+31556926);
}
try{
	$pdo = new PDO('pgsql:host='.$pgsql_host.';dbname='.$database.';port='.$port, $username, $password );
}catch(PDOException $e){
}

switch($_SERVER['REQUEST_METHOD']) {
    case "POST":
        $post = json_decode($HTTP_RAW_POST_DATA);
        $result = $pdo->exec("INSERT INTO orders (fullname, pizza_id , half , cost , token , date_order) VALUES ('" .
            $post->fullname . "','" .
            $post->pizza_id . "','" .
            $post->half . "','" .
            $post->cost . "','" .
            $_COOKIE['userToken'] . "', CURRENT_DATE)");
        break;
    case "PUT":
        $post_vars = file_get_contents("php://input");

        $post = json_decode($post_vars);
        $result = $pdo->exec("UPDATE orders SET fullname='" .
            $post->fullname . "', pizza_id='" .
            $post->pizza_id . "', half='" .
            (($post->half)?"true":"false") . "', cost='" .
            $post->cost . "' WHERE id=" . $post->id);
        break;
    case "GET":

        $arr = array();
        $result = $pdo->query('SELECT * FROM orders WHERE date_order=CURRENT_DATE ORDER BY id');
        if ($result->rowCount() > 0) {
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    if ($row['token'] == $_COOKIE['userToken']) {
                        $row['myOrder'] = true;
                    } else {
                        $row['myOrder'] = false;
                    }
                    unset($row['token']);
                    unset($row['Date']);
                    $arr[] = $row;
                }


                echo json_encode($arr);
        }else{
            echo '[]';
        }
        break;
    case "DELETE":
            $post_vars = file_get_contents("php://input");
        
            $post = json_decode($post_vars);
            $result =$pdo->query('DELETE FROM orders WHERE id='.$post->id);
        break;    
}
